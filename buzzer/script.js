$(document).ready(function () {
  var activecolor;
  firebase.database().ref("/buzzer/color").on("value", function (snapshot) {
    activecolor = snapshot.val();
    if (activecolor === "red") {
      $("#buttons").hide();
      $("body").css("background-color", "red");
      $("#table").css("background-color", "red");
    } else if (activecolor == "blue") {
      $("#buttons").hide();
      $("body").css("background-color", "blue");
      $("#table").css("background-color", "blue");
    } else {
      $("body").css("background-color", "white");
      $("#table").css("background-color", "lightgray");
      $("#buttons").fadeIn();
    }
  });

  firebase.database().ref("/buzzer/table").on("value", function (snapshot) {
    var table = snapshot.val();
    if (table == "show") {
      $("#table").fadeIn();
    } else {
      $("#table").fadeOut();
    }
  });

  var activecolor2;
  firebase.database().ref("/buzzer/tc").on("value", function (snapshot) {
    activecolor2 = snapshot.val();
    $.each(activecolor2, function(index, value) {
      if (value == "red") {
        $("#"+index).css("background-color", "red");
      } else if (value == "blue") {
        $("#"+index).css("background-color", "blue");
      } else {
        $("#"+index).css("background-color", "white");
      }
    });
  });

  $("td").mousedown(function () {
    var id = event.target.id
    $("#"+id).css("background-color", activecolor);
    firebase.database().ref("/buzzer/tc/"+id).set(activecolor);
  });



  $("#redbutton").mousedown(function () {
    $("#buttons").hide();
    $("body").css("background-color", "red");
    var audio = document.getElementById("audio");
    audio.play();
    firebase.database().ref("/buzzer/").update({
      color: "red"
    });
  });

  $("#bluebutton").mousedown(function () {
    $("#buttons").hide();
    $("body").css("background-color", "blue");
    var audio2 = document.getElementById("audio2");
    audio2.play();
    firebase.database().ref("/buzzer/").update({
      color: "blue"
    });
  });
});