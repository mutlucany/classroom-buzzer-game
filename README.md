# Classroom Buzzer Game
A competitive team play tool for in-person or remote classrooms.

**Notice:** This software is not ready for the end user. If you're a teacher without any knowledge of the software (JavaScript and Firebase) feel free to reach me at mtlcnylmz@gmail.com. I will help free of charge provided that you are not working for a private school.
## Requirements
- **buzzer** - A mobile computing device with a touchscreen and Internet connection such as a tablet or laptop to be used on a desk accessible to 2 contestants. **OR** for remote classrooms, the website opened on students' devices.
- **tv** - A computing device with a big screen visible to the whole class. **OR** for remote classrooms, the website is displayed in the presenter.

## How to play
1. There is a red team and a blue team.
2. One player from each team comes to the desk with the **Device A** on it.
3. A question is asked.
4. Whoever knows the answer, taps the buzzer of their team colour.
5. Whoever was first, the team colour will be displayed and team members answers.
6. If the answer is correct, the player chooses a square from the 4x4 area. It will be painted with the player's team colour. 
7. Game continues with a new question. When the 4x4 area is filled with the same colour as the team vertically, horizontally or diagonally, that team wins!

![](instructions.png)

## Code
This software is written in 2017 for one-time use in a classroom. Please don't yell at me for using jQuery. There is no supervision panel and some changes need to be made directly to the database while running the game. It is also not designed to work on multiple instances. 

**Firebase Realtime Database** is used as a database solution.
