$(document).ready(function () {
  var activecolor;
  firebase.database().ref("/buzzer/color").on("value", function (snapshot) {
    activecolor = snapshot.val();
    if (activecolor === "red") {
      $("body").css("background-color", "red");
      $("#table").css("background-color", "red");
    } else if (activecolor === "blue") {
      $("#buttons").hide();
      $("body").css("background-color", "blue");
      $("#table").css("background-color", "blue");
    } else {
      $("body").css("background-color", "white");
      $("#table").css("background-color", "lightgray");
    }
  });


  var activecolor2;
  firebase.database().ref("/buzzer/tc").on("value", function (snapshot) {
    activecolor2 = snapshot.val();
    $.each(activecolor2, function(index, value) {
      if (value === "red") {
        $("#"+index).css("background-color", "red");
      } else if (value === "blue") {
        $("#"+index).css("background-color", "blue");
      } else {
        $("#"+index).css("background-color", "white");
      }
    });
  });

  document.onkeydown = checkKey;

  function checkKey(e) {

    e = e || window.event;

    if (e.keyCode === '38') {
      // up arrow
      firebase.database().ref("/buzzer/").update({
        table: "show"
      });
    }
    else if (e.keyCode === '40') {
      // down arrow
      firebase.database().ref("/buzzer/").update({
        table: "hide"
      });
    }
    else if (e.keyCode === '37') {
      // left arrow
      firebase.database().ref("/buzzer/").update({
        color: "red"
      });
    }
    else if (e.keyCode === '39') {
      // right arrow
      firebase.database().ref("/buzzer/").update({
        color: "blue"
      });
    }

    else if (e.keyCode === '32') {
      // right arrow
      firebase.database().ref("/buzzer/").update({
        color: "none"
      });
    }

  }
});